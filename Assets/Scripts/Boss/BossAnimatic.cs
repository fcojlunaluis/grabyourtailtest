using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossAnimatic : MonoBehaviour
{
    public Animator anim;
    //float timeAnimation = 10f;
    //float actualTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim.SetTrigger("trigger");
    }

    // Update is called once per frame
    void Update()
    {
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                //actualTime = 0;
                anim.SetTrigger("trigger");
                anim.SetInteger("index", Random.Range(0, 4));
            }
    }
}

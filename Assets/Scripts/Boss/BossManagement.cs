using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossManagement : MonoBehaviour
{
    public int totalLife = 10000;
    public int damagePerBullet = 10;
    public HealtBar healt;
    private int actualLife;

    public Animator anim;
    public GameObject bigTail;

    //float time = 0;

    // Start is called before the first frame update
    void Start()
    {
        actualLife = totalLife;
        healt.setMaxValueHealt(totalLife);
    }

    private void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            actualLife -= damagePerBullet;
            healt.setHealth(actualLife);
            if (actualLife <= 0)
            {
                Destroy(this.gameObject);
                GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
                for (int i = 0; i < enemys.Length;i++)
                {
                    Destroy(enemys[i]);
                }

                SceneManager.LoadScene("GameOver");
                //iniciarMuerte();
            }
        }
        
    }

    void iniciarMuerte()
    {
        GameObject tmp = Instantiate(bigTail, this.gameObject.transform.position, Quaternion.identity);
        tmp.GetComponent<Animator>().SetBool("Die",true);
        new WaitForSeconds(3);
        tmp.GetComponent<Animator>().SetInteger("index", 5);
    }
}

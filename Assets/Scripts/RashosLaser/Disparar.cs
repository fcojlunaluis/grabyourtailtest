using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public GameObject rashoLaser;
    public GameObject shooter;
    public float speed = 5f;
    public float cadencia = 0.5f;
    //private float shotRateTime = 0;
    public Transform _puntoSalida;
    // Start is called before the first frame update
    void Start()
    {
        _puntoSalida = transform.Find("PuntoSalida");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            shoot();
        }
    }

    public void shoot()
    {
        if (rashoLaser != null && _puntoSalida != null && shooter != null)
        {
            Quaternion rotation = Quaternion.Euler(90f, 0, 0); ;
            GameObject myBullet = Instantiate(rashoLaser, _puntoSalida.position, rotation);
            myBullet.GetComponent<Rigidbody>().MovePosition((_puntoSalida.transform.forward * speed)* Time.deltaTime);

            /*if (Time.time > shotRateTime)
            {
                Quaternion rotation = Quaternion.Euler(90f,0,0); ;
                GameObject myBullet = Instantiate(rashoLaser, _puntoSalida.position, rotation);
                myBullet.GetComponent<Rigidbody>().AddForce((_puntoSalida.transform.forward * speed));

                shotRateTime = Time.time + cadencia;
            }*/


        }
    }
}

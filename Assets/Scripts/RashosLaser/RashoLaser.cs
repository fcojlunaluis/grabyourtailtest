using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RashoLaser : MonoBehaviour
{

    public Vector3 direction;
    public float _livingTime = 3f;
    private float _startTime;


    // Start is called before the first frame update
    void Start()
    {
        _startTime = Time.time;

        Destroy(this.gameObject, _livingTime);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int playerLife = 100;
    public int actualLife;
    public HealtBar healt;
    public int damage = 5;
    public Color initialColor;
    public Color finalColor;
    // Start is called before the first frame update
    void Start()
    {
        actualLife = playerLife;
        healt.setMaxValueHealt(playerLife);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Whos touching the child?!!");
            takeDamage(damage);
        }
    }

    void takeDamage(int damage)
    {
        actualLife -= damage;
        healt.setHealth(actualLife);
    }

    void ligthRed()
    {

    }
}

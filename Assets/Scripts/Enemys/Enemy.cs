using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator anim;
    // Start is called before the first frame update
    private void Awake()
    {
        anim = GetComponent<Animator>();
        anim.SetTrigger("OnCreate");
        anim.SetInteger("index", Random.Range(0, 5));
    }


}

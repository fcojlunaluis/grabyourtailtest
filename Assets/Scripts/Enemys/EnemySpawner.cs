using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public Transform player;
    public Transform spawnPoint;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 2f)
        {
            float x = Random.Range(player.position.x-30, player.position.x + 30);
            float y = Random.Range(player.position.y-30, player.position.y + 50);
            float z = Random.Range(player.position.z + 30, player.position.z + 80);
            timer = 0;
            Quaternion rotasion = new Quaternion();
            if (GameObject.FindGameObjectsWithTag("Enemy").Length < 20)
            {
                Instantiate(enemyPrefab, new Vector3(x, y, z), rotasion);
                //Instantiate(enemyPrefab, spawnPoint.transform.position , rotasion);
            }
        }
    }
}

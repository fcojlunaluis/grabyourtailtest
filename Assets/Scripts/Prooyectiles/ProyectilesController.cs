 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilesController : MonoBehaviour
{
    public Camera cam;
    public GameObject projectile;
    public Transform firepoint;
    public float projectileSpeed = 5f;
    public float fireRate = 4f;

    private Vector3 destination;
    private float timeToFire;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1")&&Time.time >= timeToFire)
        {
            timeToFire = Time.time + 1 / fireRate;
            shootProjectile();
        }
    }

    void shootProjectile()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            destination = hit.point;
            InstantiateProjectile();
        }
        else
        {
            destination = ray.GetPoint(1000);
            InstantiateProjectile();
        }
    }

    void InstantiateProjectile()
    {
        var projectileObj = Instantiate(projectile, firepoint.position, Quaternion.identity) as GameObject;
        projectileObj.GetComponent<Rigidbody>().velocity = (destination - firepoint.position).normalized * projectileSpeed;
    }
}

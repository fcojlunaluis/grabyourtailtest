using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float lifeTime;
    public GameObject impacto;
    private bool collide;

    private void Start()
    {
        Destroy(this.gameObject, lifeTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Bullet" && collision.gameObject.tag != "Player" && collide)
        {
            collide = true;
            var impac = Instantiate(impacto,collision.contacts[0].point, Quaternion.identity ) as GameObject;
            Destroy(impac, 3);
            Destroy(this.gameObject);
        }
        
    }
}
